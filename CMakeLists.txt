cmake_minimum_required(VERSION 3.0.2)
project(apl_msgs)

find_package(catkin REQUIRED COMPONENTS
  genmypy
  message_generation
  rospy
  std_msgs
)

add_message_files(
  FILES
  Adc.msg
  CharucoDetection.msg
  Ctd.msg
  DissolvedOxygen.msg
  Int16Stamped.msg
  JointPositionCommand.msg
  LeakDetection.msg
  LeakVoltage.msg
  LightBrightness.msg
  SetLightLevel.msg
  IncrementLightLevel.msg
  PowerCommand.msg
  PowerStatus.msg
  RawData.msg
  SeapointTurbidity.msg
  StringStamped.msg
  UInt16Stamped.msg
)

generate_messages(
  DEPENDENCIES
  std_msgs
)

catkin_package(
  CATKIN_DEPENDS genmypy message_runtime rospy std_msgs
)

catkin_install_python(PROGRAMS
  nodes/echo_raw_data
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)
